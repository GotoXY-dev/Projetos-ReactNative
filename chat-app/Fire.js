import firebase from 'firebase'

class Fire {
    constructor(){
        this.init()
        this.checkAuth()
    }

    init = () => {
        if(!firebase.apps.length){
            firebase.initializeApp({
                
                    apiKey: "AIzaSyDSHDLFZik6xEiDpNFnNbbotQSsRhtXquo",
                    authDomain: "react-native-chat-clone-8b46e.firebaseapp.com",
                    projectId: "react-native-chat-clone-8b46e",
                    storageBucket: "react-native-chat-clone-8b46e.appspot.com",
                    messagingSenderId: "269197012094",
                    appId: "1:269197012094:web:a0395295f213548648fc2f",
                    measurementId: "G-N3BGHBQ2ZP"
                  
            })
        }
    };

    checkAuth = () => {
        firebase.auth().onAuthStateChanged(user => {
            if(!user){
                firebase.auth().signInAnonymously();
            }
        });
    };  

    send = message => {
        message.forEach(item => {
            const message = {
                text: item.text,
                timestamp: firebase.database.ServerValue.TIMESTAMP,
                user: item.user
            }

            this.db.push(message)
        });
    };

    parse = message => {
        const {user, text, timestamp} = message.val()
        const {key: _id} = message 
        const createdAt = new Date(timestamp)

        return{
            _id,
            createAt,
            text,
            user
        };
    };

    get = callback =>{
        this.db.on('child_added', snapshop => callback(this.parse(snapshop)));      
    }

    off(){
        this.db.off()
    }
    get db(){
       return firebase.database().ref("message");     
    }

    get uid(){
        return (firebase.auth().currentUser || {}.uid)
    }
}

export default new fire();