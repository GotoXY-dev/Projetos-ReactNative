import React from "react";
import { Platform, KeyboardAvoidingView, SafeAreaView} from "react-native";
import {GiftedChat} from 'react-native-gifted-chat'
import Fire from '../Fire';

export default class ChatScreen extends React.Component{

	state = {
		message: []
	}

	get user(){
		return {
			_id: Fire.uid,
			name: this.props.navigation.state.params.name
		}
	}

	componentDidMount() {
		Fire.get(message => this.setState(previous => ({
			message: GiftedChat.append(previous.message, message)
		}))
		);
	}

	componentWillUnmount(){
		Fire.off();
	}

	render(){
		const chat = <GiftedChat message={this.state.message} onSend={Fire.send} user={this.user} />
		if(Platform.OS === 'android'){
			return(
				<KeyboardAvoidingView style={{flex: 1}} behavior="padding" KeyboardVerticalOffset={30} enable>
					{chat}
				</KeyboardAvoidingView>
	
			);
		}
		
	   return <SafeAreaView style={{ flex: 1}}>{chat}</SafeAreaView>;
	}
}

